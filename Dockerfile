FROM node:14.14

WORKDIR /srv/

ADD ./ ./

RUN touch ./firebase-service-account.json

RUN echo "{}" > ./firebase-service-account.json

RUN yarn install

RUN yarn build

EXPOSE 80

ENTRYPOINT NODE_ENV=production node ./dist/build.js
