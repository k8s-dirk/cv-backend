import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import * as yup from 'yup';
import * as admin from 'firebase-admin';
const serviceAccount = require('../firebase-service-account.json');

/*
    Some constants
*/

/*
    Creating instances
*/

const credentials = process.env.FIREBASE_AUTH
    ? JSON.parse(Buffer.from(process.env.FIREBASE_AUTH, 'base64').toString())
    : serviceAccount;

admin.initializeApp({
    credential: admin.credential.cert(credentials),
    databaseURL:
        'https://cv-backend-5e80d-default-rtdb.europe-west1.firebasedatabase.app',
});
const db = admin.database();
const app = express();
const port = process.env.NODE_ENV === 'production' ? 80 : 3001;

/*
    Adding middleware to Express
    This is necessary to get the post
    data from the request and to 
    access the API from a different 
    host 
*/

app.use(bodyParser.json());
app.use(cors());

/*
    Express routes/endpoints
*/

/*
    CV endpoints
*/

const cvSchema = yup.object().shape({
    cvId: yup.string(),
    description: yup.string(),
    hobbies: yup.string(),
    references: yup.string(),
    availability: yup.bool(),
    introduction: yup.string(),
    team: yup.string(),
});

app.get('/cvs', async (req, res) => {
    try {
        const dbResult = await db.ref(`cv/`).get();
        res.status(200).json({
            success: true,
            data: dbResult.toJSON(),
        });
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.get('/cv/:cvId', async (req, res) => {
    const { cvId } = req.params;
    try {
        const dbResult = await db.ref(`cv/${cvId}`).get();
        if (dbResult.exists()) {
            res.status(200).json({
                success: true,
                data: { ...dbResult.toJSON(), id: dbResult.key },
            });
        } else {
            res.status(404).json({
                success: false,
                error: `CV with ID ${cvId} not found`,
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/cv', async (req, res) => {
    const { cvId } = req.body;

    const validEntry = await cvSchema.isValid(req.body);
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const cv = {
        description: req.body?.description ?? null,
        hobbies: req.body?.hobbies ?? null,
        references: req.body?.references ?? null,
        availability: req.body?.availability ?? null,
        introduction: req.body?.introduction ?? null,
        team: req.body?.team ?? null,
        lastUpdated: new Date().getTime(),
    };

    try {
        if (cvId) {
            const existingCv = await db.ref(`cv/${cvId}`).get();

            if (existingCv.exists()) {
                await existingCv.ref.update(cv);
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: 'Person not found',
                });
            }
        } else {
            const dbResult = await db.ref('cv/').push(cv);
            res.status(200).json({ success: true, id: dbResult.key });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.delete('/cv/:cvId', async (req, res) => {
    const { cvId } = req.params;

    try {
        if (cvId) {
            const existingTodo = await db.ref(`cv/${cvId}`).get();

            if (existingTodo.exists()) {
                await existingTodo.ref.remove();
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `CV with ${cvId} not found`,
                });
            }
        } else {
            throw new Error();
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

/*
    Person endpoints
*/
const personSchema = yup.object().shape({
    firstName: yup.string().required(),
    lastName: yup.string().required(),
    age: yup.number().required(),
    address: yup.object().shape({
        street: yup.string().required(),
        zipCode: yup.string().required(),
        houseNumber: yup.string().required(),
        city: yup.string().required(),
        country: yup.string().required(),
    }),
    dateOfBirth: yup.string().required(),
    phoneNumber: yup.string().required(),
    email: yup.string().required(),
    driversLicense: yup.bool().required(),
    driversLicenseExtra: yup.string(),
    gender: yup.string(),
    rate: yup.number(),
    willingnessToWorkAbroad: yup
        .mixed()
        .oneOf(['netherlands', 'benelux', 'europe', 'global']),
});

app.get('/cv/:cvId/person', async (req, res) => {
    const { cvId } = req.params;
    try {
        const dbResult = await db.ref(`cv/${cvId}/person`).get();

        if (dbResult.exists()) {
            res.status(200).json({
                success: true,
                data: dbResult.toJSON(),
            });
        } else {
            res.status(404).json({
                success: false,
                error: `Person of CV with ID ${cvId} not found`,
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/cv/:cvId/person', async (req, res) => {
    const { cvId } = req.params;

    const validEntry = await personSchema.isValid({ ...req.body, cvId });
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const person = {
        firstName: req.body?.firstName ?? null,
        lastName: req.body?.lastName ?? null,
        age: req.body?.age ?? null,
        address: req.body?.address ?? null,
        dateOfBirth: req.body?.dateOfBirth ?? null,
        phoneNumber: req.body?.phoneNumber ?? null,
        email: req.body?.email ?? null,
        driversLicense: req.body?.driversLicense ?? null,
        driversLicenseExtra: req.body?.driversLicenseExtra ?? null,
        gender: req.body?.gender ?? null,
        rate: req.body?.rate ?? null,
        willingnessToWorkAbroad: req.body?.willingnessToWorkAbroad ?? null,
    };

    try {
        const existingCv = await db.ref(`cv/${cvId}`).get();

        if (existingCv.exists()) {
            await existingCv.ref.child('person').update(person);
            res.status(200).json({ success: true });
        } else {
            res.status(404).json({
                success: false,
                error: 'CV not found',
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

/*
    Job endpoints
*/

const jobSchema = yup.object().shape({
    jobId: yup.string(),
    from: yup.string().required(),
    until: yup.string().required(),
    function: yup.string().required(),
    description: yup.string().required(),
    companyName: yup.string(),
});

app.get('/cv/:cvId/jobs', async (req, res) => {
    const { cvId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/jobs`).get();
        res.status(200).json({
            success: true,
            data: dbResult.toJSON(),
        });
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.get('/cv/:cvId/job/:jobId', async (req, res) => {
    const { cvId, jobId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/jobs/${jobId}`).get();
        if (dbResult.exists()) {
            res.status(200).json({
                success: true,
                data: { ...dbResult.toJSON(), id: dbResult.key },
            });
        } else {
            res.status(404).json({
                success: false,
                error: `Job with ${jobId} of CV with cvId ${cvId} not found`,
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/cv/:cvId/job', async (req, res) => {
    const { cvId } = req.params;
    const { jobId } = req.body;

    const validEntry = await jobSchema.isValid({ ...req.body, cvId });
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const job = {
        from: req.body?.from ?? null,
        until: req.body?.until ?? null,
        function: req.body?.function ?? null,
        description: req.body?.description ?? null,
        companyName: req.body?.companyName ?? null,
    };

    try {
        if (jobId && cvId) {
            const existingJob = await db.ref(`cv/${cvId}/jobs/${jobId}`).get();

            if (existingJob.exists()) {
                await existingJob.ref.update(job);
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `Job with ${jobId} of CV with cvId ${cvId} not found`,
                });
            }
        } else {
            const existingCv = await db.ref(`cv/${cvId}`).get();
            if (existingCv.exists()) {
                const result = await existingCv.ref.child('jobs').push(job);
                res.status(200).json({ success: true, id: result.key });
            } else {
                res.status(404).json({
                    success: false,
                    error: `CV with cvId ${cvId} not found`,
                });
            }
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

/*
    Education endpoints
*/

const educationSchema = yup.object().shape({
    educationId: yup.string(),
    from: yup.string().required(),
    until: yup.string().required(),
    gratuated: yup.bool().required(),
    schoolName: yup.string().required(),
    educationName: yup.string().required(),
    description: yup.string().required(),
});

app.get('/cv/:cvId/education', async (req, res) => {
    const { cvId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/education`).get();
        res.status(200).json({
            success: true,
            data: dbResult.toJSON(),
        });
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.get('/cv/:cvId/education/:educationId', async (req, res) => {
    const { cvId, educationId } = req.params;

    try {
        const dbResult = await db
            .ref(`cv/${cvId}/education/${educationId}`)
            .get();
        if (dbResult.exists()) {
            res.status(200).json({
                success: true,
                data: { ...dbResult.toJSON(), id: dbResult.key },
            });
        } else {
            res.status(404).json({
                success: false,
                error: `Education with ${educationId} of CV with cvId ${cvId} not found`,
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/cv/:cvId/education', async (req, res) => {
    const { cvId } = req.params;
    const { educationId } = req.body;

    const validEntry = await educationSchema.isValid({ ...req.body, cvId });
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const education = {
        from: req.body?.from ?? null,
        until: req.body?.until ?? null,
        gratuated: req.body?.gratuated ?? null,
        schoolName: req.body?.schoolName ?? null,
        educationName: req.body?.educationName ?? null,
        description: req.body?.description ?? null,
    };

    try {
        if (educationId && cvId) {
            const existingJob = await db
                .ref(`cv/${cvId}/education/${educationId}`)
                .get();

            if (existingJob.exists()) {
                await existingJob.ref.update(education);
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `Education with ${educationId} of CV with cvId ${cvId} not found`,
                });
            }
        } else {
            const existingCv = await db.ref(`cv/${cvId}`).get();
            if (existingCv.exists()) {
                const result = await existingCv.ref
                    .child('education')
                    .push(education);
                res.status(200).json({ success: true, id: result.key });
            } else {
                res.status(404).json({
                    success: false,
                    error: `CV with cvId ${cvId} not found`,
                });
            }
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

/*
    Course endpoints
*/

const courseSchema = yup.object().shape({
    courseId: yup.string(),
    year: yup.string().required(),
    courceName: yup.string().required(),
    certificate: yup.bool().required(),
    description: yup.string().required(),
});

app.get('/cv/:cvId/courses', async (req, res) => {
    const { cvId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/course`).get();
        res.status(200).json({
            success: true,
            data: dbResult.toJSON(),
        });
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.get('/cv/:cvId/course/:courseId', async (req, res) => {
    const { cvId, courseId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/crouses/${courseId}`).get();
        if (dbResult.exists()) {
            res.status(200).json({
                success: true,
                data: { ...dbResult.toJSON(), id: dbResult.key },
            });
        } else {
            res.status(404).json({
                success: false,
                error: `Course with ${courseId} of CV with cvId ${cvId} not found`,
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/cv/:cvId/course', async (req, res) => {
    const { cvId } = req.params;
    const { courseId } = req.body;

    const validEntry = await courseSchema.isValid({ ...req.body, cvId });
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const course = {
        year: req.body?.year ?? null,
        courceName: req.body?.courceName ?? null,
        certificate: req.body?.certificate ?? null,
        description: req.body?.description ?? null,
    };

    try {
        if (courseId && cvId) {
            const existingJob = await db
                .ref(`cv/${cvId}/crouses/${courseId}`)
                .get();

            if (existingJob.exists()) {
                await existingJob.ref.update(course);
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `Course with ${courseId} of CV with cvId ${cvId} not found`,
                });
            }
        } else {
            const existingCv = await db.ref(`cv/${cvId}`).get();
            if (existingCv.exists()) {
                const result = await existingCv.ref
                    .child('courses')
                    .push(course);
                res.status(200).json({ success: true, id: result.key });
            } else {
                res.status(404).json({
                    success: false,
                    error: `CV with cvId ${cvId} not found`,
                });
            }
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

/*
    Skill endpoints
*/

const skillSchema = yup.object().shape({
    skillId: yup.string(),
    name: yup.string().required(),
    description: yup.string().required(),
});

app.get('/cv/:cvId/skills', async (req, res) => {
    const { cvId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/skills`).get();
        res.status(200).json({
            success: true,
            data: dbResult.toJSON(),
        });
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.get('/cv/:cvId/skill/:skillId', async (req, res) => {
    const { cvId, skillId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/skills/${skillId}`).get();
        if (dbResult.exists()) {
            res.status(200).json({
                success: true,
                data: { ...dbResult.toJSON(), id: dbResult.key },
            });
        } else {
            res.status(404).json({
                success: false,
                error: `Skill with ${skillId} of CV with cvId ${cvId} not found`,
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/cv/:cvId/skill', async (req, res) => {
    const { cvId } = req.params;
    const { skillId } = req.body;

    const validEntry = await skillSchema.isValid({ ...req.body, cvId });
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const skill = {
        name: req.body?.name ?? null,
        description: req.body?.description ?? null,
    };

    try {
        if (skillId && cvId) {
            const existingJob = await db
                .ref(`cv/${cvId}/skills/${skillId}`)
                .get();

            if (existingJob.exists()) {
                await existingJob.ref.update(skill);
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `Skill with ${skillId} of CV with cvId ${cvId} not found`,
                });
            }
        } else {
            const existingCv = await db.ref(`cv/${cvId}`).get();
            if (existingCv.exists()) {
                const result = await existingCv.ref.child('skills').push(skill);
                res.status(200).json({ success: true, id: result.key });
            } else {
                res.status(404).json({
                    success: false,
                    error: `CV with cvId ${cvId} not found`,
                });
            }
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

/*
    Language endpoints
*/

const languageSchema = yup.object().shape({
    languageId: yup.string(),
    languageName: yup.string().required(),
    speakingSkill: yup.string().required(),
    writingSkill: yup.string().required(),
    readingSkill: yup.string().required(),
});

app.get('/cv/:cvId/languages', async (req, res) => {
    const { cvId } = req.params;

    try {
        const dbResult = await db.ref(`cv/${cvId}/languages`).get();
        res.status(200).json({
            success: true,
            data: dbResult.toJSON(),
        });
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.get('/cv/:cvId/language/:languageId', async (req, res) => {
    const { cvId, languageId } = req.params;

    try {
        const dbResult = await db
            .ref(`cv/${cvId}/languages/${languageId}`)
            .get();
        if (dbResult.exists()) {
            res.status(200).json({
                success: true,
                data: { ...dbResult.toJSON(), id: dbResult.key },
            });
        } else {
            res.status(404).json({
                success: false,
                error: `Language with ${languageId} of CV with cvId ${cvId} not found`,
            });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/cv/:cvId/language', async (req, res) => {
    const { cvId } = req.params;
    const { languageId } = req.body;

    const validEntry = await languageSchema.isValid({ ...req.body, cvId });
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const language = {
        languageName: req.body?.languageName ?? null,
        speakingSkill: req.body?.speakingSkill ?? null,
        writingSkill: req.body?.writingSkill ?? null,
        readingSkill: req.body?.readingSkill ?? null,
    };

    try {
        if (languageId && cvId) {
            const existingJob = await db
                .ref(`cv/${cvId}/languages/${languageId}`)
                .get();

            if (existingJob.exists()) {
                await existingJob.ref.update(language);
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `Language with ${languageId} of CV with cvId ${cvId} not found`,
                });
            }
        } else {
            const existingCv = await db.ref(`cv/${cvId}`).get();
            if (existingCv.exists()) {
                const result = await existingCv.ref
                    .child('languages')
                    .push(language);
                res.status(200).json({ success: true, id: result.key });
            } else {
                res.status(404).json({
                    success: false,
                    error: `CV with cvId ${cvId} not found`,
                });
            }
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

/*
    Todos endpoints
*/

const todoSchema = yup.object().shape({
    todoId: yup.string(),
    taskName: yup.string().required(),
    done: yup.bool().required(),
    urgent: yup.bool().required(),
});

app.get('/todos', async (req, res) => {
    try {
        const dbResult = await db.ref(`todos/`).get();
        res.status(200).json({
            success: true,
            data: dbResult.toJSON(),
        });
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.post('/todo', async (req, res) => {
    const { todoId } = req.body;

    const validEntry = await todoSchema.isValid({ ...req.body, todoId });
    if (!validEntry) {
        res.status(400).json({
            success: false,
            error: 'The provided information was not correct',
        });
    }

    const todo = {
        creationDate: new Date().toLocaleDateString('nl-NL'),
        creationTime: new Date().toLocaleTimeString('nl-NL'),
        taskName: req.body?.taskName ?? null,
        done: req.body?.done ?? null,
        urgent: req.body?.urgent ?? null,
    };

    try {
        if (todoId) {
            const existingTodo = await db.ref(`todos/${todoId}`).get();

            if (existingTodo.exists()) {
                await existingTodo.ref.update(todo);
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `Todo with ${todoId} not found`,
                });
            }
        } else {
            const dbResult = await db.ref('todos/').push(todo);
            res.status(200).json({ success: true, id: dbResult.key });
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.delete('/todo/:todoId', async (req, res) => {
    const { todoId } = req.params;

    try {
        if (todoId) {
            const existingTodo = await db.ref(`todos/${todoId}`).get();

            if (existingTodo.exists()) {
                await existingTodo.ref.remove();
                res.status(200).json({ success: true });
            } else {
                res.status(404).json({
                    success: false,
                    error: `Todo with ${todoId} not found`,
                });
            }
        } else {
            throw new Error();
        }
    } catch (e) {
        res.status(500).json({ success: false, error: e.message });
    }
});

app.get('/', async (req, res) => {
    res.status(200).send("I'm alive");
});

/*
    Starting the app
*/
app.listen(port, () =>
    console.log(`CV-backend service is running on port ${port}!`)
);
