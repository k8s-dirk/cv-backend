export type Person = {
    firstName: string;
    lastName: string;
    age: number;
    address: {
        street: string;
        zipCode: string;
        houseNumber: string;
        city: string;
        country: string;
    };
    dateOfBirth: string;
    phoneNumber: string;
    email: string;
    driversLicense: boolean;
    driversLicenseExtra: string;
};

export type CV = {
    description: string;
    person: Person;
    jobs: Job[];
    education: Education[];
    courses: Course[];
    skills: Skill[];
    hobbies: string;
    references: string;
    languages: Language[];
};

export type Job = {
    from: string;
    until: string;
    function: string;
    description: string;
};

export type Education = {
    from: string;
    until: string;
    gratuated: boolean;
    schoolName: string;
    educationName: string;
    description: string;
};

export type Course = {
    year: string;
    courceName: string;
    certificate: boolean;
    description: string;
};

export type Skill = {
    name: string;
    description: string;
};

export type Language = {
    languageName: string;
    speakingSkill: string;
    writingSkill: string;
    readingSkill: string;
};
